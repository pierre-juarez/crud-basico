<?php include("conexion.php"); ?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrapp, en su versión mimificado (min) -->
	<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
	<!-- Font awesome, fuente de iconos para integrar a cualquier sitio web -->
	<link rel="stylesheet" href="fonts/font-awesome-v4.7.0/css/font-awesome.min.css">
	<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="css/estilos.css">
	<title>CRUD BÁSICO👏👇💪😎</title>
</head>
<body>

	<h3 class="text-center my-4">CRUD BÁSICO CON PHP, JQUERY Y MYSQL👏👇💪😎</h3><br>

	<div class="dark-mode">
		Modo noche
		<label class="switch">
			<input id="checkbox" type="checkbox">
			<span class="slider round"></span>
		</label>
	</div>

	<center>
	<button class="btn btn-primary mx-4 my-4"  data-toggle="modal" data-target="#ejemploModal">Nuevo docente <i class="fa fa-user-plus"></i></button>
	
	<table class="table-bordered my-4 text-center" id="tablaDocente">
		<thead>
			<tr>
				<th>ID</th>
				<th>PROFESOR</th>
				<th>ASIGNATURA</th>
				<th>TUTORÍA</th>
				<th>INSTRUCCIÓN</th>
				<th>ACCIONES</th>

			
			</tr>	
		</thead>
		<tbody>
			<?php 
			
				$sql = "CALL LISTAR_DOCENTES('')";
				$result=mysqli_query($conexion,$sql);
				while($fila=mysqli_fetch_assoc($result)){
					
			?>
			<tr>
				<td><?php echo $fila['ID'] ?></td>
				<td><?php echo $fila['PROFESOR'] ?></td>
				<td><?php echo $fila['ASIGNATURA'] ?></td>
				<td><?php echo $fila['TUTORIA'] ?></td>
				<td><?php echo $fila['INSTRUCCION'] ?></td>
				<td> 
					<button class="btn btn-info" onclick="modalEditar('<?php echo $fila['ID'] ?>');">
						<!-- Icono de editar con FontAwesome -->
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</button>
					<button class="btn btn-danger"	onclick="modalEliminar('<?php echo $fila['ID'] ?>');">
						<!-- Icono de eliminar con FontAwesome -->
						<i class="fa fa-trash-o" aria-hidden="true" id=""></i>
					</button>
				</td>
			<?php } ?>
			</tr>
		</tbody>
	</table>

	</center>
	<!-- Ventana Modal creada con Bootstrap -->

	<!-- Modal Nuevo Docente -->
	<div class="modal fade" id="ejemploModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Agregar docente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<label for="profesor">Docente</label>
				<input type="text" id="profesor" class="form-control input-md">
		
				<label for="asignatura">Asignatura</label>
				<input type="text" id="asignatura" class="form-control input-md">

				<label for="tutoria">Tutoría</label>
				<input type="text" id="tutoria" class="form-control input-md">

				<label for="instruccion">Instrucción</label>
				<input type="text" id="instruccion" class="form-control input-md">
		</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" id="btnNuevoDocente">Agregar docente</button>
			</div>
			</div>
		</div>
	</div>

	<!-- Modal Editar Docente -->
	<div class="modal fade" id="modalEditar">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar docente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="codigo-actualizar">
				
				<label for="profesor-actualizar">Docente</label>
				<input type="text" id="profesor-actualizar" class="form-control input-md">
		
				<label for="asignatura-actualizar">Asignatura</label>
				<input type="text" id="asignatura-actualizar" class="form-control input-md">

				<label for="tutoria-actualizar">Tutoría</label>
				<input type="text" id="tutoria-actualizar" class="form-control input-md">

				<label for="instruccion-actualizar">Instrucción</label>
				<input type="text" id="instruccion-actualizar" class="form-control input-md">
		</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-info" id="btnEditarDocente">Editar docente</button>
			</div>
			</div>
		</div>
	</div>

		<!-- Modal Eliminar Docente -->
	<div class="modal fade" id="modalEliminar">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Eliminar docente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="codEliminar">
				<label>El docente se eliminará de la base de datos</label>
			</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="btnEliminarDocente">¡Sí, eliminar docente!</button>
				</div>
				</div>
			</div>
	</div>

	<div class="footer my-4">
		<div class="container">
			<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 text-center">
			<p>Copyright 2021 © Creado y diseñado con ❤ por <a href="https://pierre-juarez.gitlab.io/" target="_blank" class="font-weight-bold">Pierre Juarez U.</a></p>
			</div>     
			<div class="col-sm-3"></div>
			</div>
		</div>
	</div>

<!-- Scripts necesario para que cargue los modals -->
<script src="js/jquery/jquery.min.js"></script>	
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- Script que usaremos para enviar y agregar datos -->
<script src="js/funciones-crud.js"></script>

</body>
</html>