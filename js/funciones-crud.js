$("#btnNuevoDocente").click(function(){
	agregaDocente();
});

$("#btnEditarDocente").click(function(){
	editarDocente();
});

$("#btnEliminarDocente").click(function(){
	eliminarDocente();
});

const chk = document.querySelector("#checkbox");
if(window.matchMedia('(prefers-color-scheme: dark)').matches){
    chk.setAttribute('checked',true);
}

chk.addEventListener('change',function(e) {
    document.body.classList.toggle('is-dark-mode');
    if(this.checked){
        document.body.classList.remove('is-light-mode');
        document.body.classList.add('is-dark-mode');
    }else{
        document.body.classList.remove('is-dark-mode');
        document.body.classList.add('is-light-mode');
    }
});



function agregaDocente(){
	

	var data = {  
		 profesor : $("#profesor").val(),
		 asignatura : $("#asignatura").val(),
		 tutoria : $("#tutoria").val(),
		 instruccion : $("#instruccion").val()
	};

	$.ajax({
		type:"POST",
		url:"agregarDocente.php",
		dataType: "JSON",
		data:data,
		success:function(respuesta){
			if(respuesta.estado){
				alert(respuesta.msj);
				$("#ejemploModal").modal("hide");
				location.reload();
			}else{
				alert(respuesta.msj);
			}
		}
	});

}

function modalEditar(codigo){
	
	var codigo = codigo;

	$.getJSON("getDocentes.php",{codigo:codigo},function(data){
		$.each(data,function(index,value){

		  $("#codigo-actualizar").val(value.ID);
		  $("#profesor-actualizar").val(value.PROFESOR);
		  $("#asignatura-actualizar").val(value.ASIGNATURA);
		  $("#tutoria-actualizar").val(value.TUTORIA);
		  $("#instruccion-actualizar").val(value.INSTRUCCION);

		});
	}); 

	$("#modalEditar").modal("show");
	
}

function editarDocente(){

	var data = {  
		codigo : $("#codigo-actualizar").val(),
		profesor : $("#profesor-actualizar").val(),
		asignatura : $("#asignatura-actualizar").val(),
		tutoria : $("#tutoria-actualizar").val(),
		instruccion : $("#instruccion-actualizar").val()
   };

   $.ajax({
	   type:"POST",
	   url:"editarDocente.php",
	   dataType: "JSON",
	   data:data,
	   success:function(respuesta){
		   if(respuesta.estado){
			   alert(respuesta.msj);
			   $("#modalEditar").modal("hide");
			   location.reload();
		   }else{
			   alert(respuesta.msj);
		   }
	   }
   });

}

function modalEliminar(codigo){
	var cod = codigo;
	$("#codEliminar").val(cod);
	$("#modalEliminar").modal("show");
	
}

function eliminarDocente(){

	var data = {  
		codigo : $("#codEliminar").val()
   };

   $.ajax({
	   type:"POST",
	   url:"eliminarDocente.php",
	   dataType: "JSON",
	   data:data,
	   success:function(respuesta){
		   if(respuesta.estado){
			   alert(respuesta.msj);
			   $("#modalEliminar").modal("hide");
			   location.reload();
		   }else{
			   alert(respuesta.msj);
		   }
	   }
   });

}