
# Crud Básico, PHP, MySql y Javascript, con Dark Mode

_Integración de un CRUD (Create, Read, Update, y Delete) de elementos, usando PHP, Javascript, Bootstrap, y Fontawesome para el diseño, le apliqué un "Dark Mode", súper básico pero interesante._

Te dejo aquí una demo: https://crud-basico.000webhostapp.com/

## Screenshot Demo:

![screenshot demo](img/screenshot_demo1.png?raw=true "Screenshot Demo - Dark Mode")
![screenshot demo](img/screenshot_demo2.png?raw=true "Screenshot Demo - Ligth Mode")


## Despliegue 📦

_Reconstruya la Base de Datos desde el script ubicado en la carpeta "database", con su manejador de BD's favorito._

_Edite la configuración de la BD en el archivo "conexion.php"._
s
```
 $conexion = @mysqli_connect('nombre_servidor','usuario_bd','contrasenia_bd','nombre_de_bd');
```
## Construido con 🛠️

* [PHP 7.2.7](https://www.php.net/) - Lenguaje nativo
* [Javascript](https://developer.mozilla.org/es/docs/Web/JavaScript) - Lenguaje nativo
* [CSS](https://www.w3schools.com/css/) - Lenguaje nativo
* [JQuery v3.5.1](https://jquery.com/download/) - Librería de Javascript
* [Font awesome v4.7](https://fontawesome.com/v4.7.0/) - Fuente de iconos
* [Bootstrap 4.0](https://getbootstrap.com/docs/4.0/) - Framework de diseño web responsive design

## Licencia 📄

Este proyecto es Open Source, el código puede ser usado, mejorado, o descargado sin ningún problema, ¡recuerda compartir si te sirvió!

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invitáme un café ☕. 
* Da las gracias públicamente 🤓.
* Dale "estrellita" al proyecto.


---
⌨️ con ❤️ por [Pierre Juarez](https://gitlab.com/pierre-juarez) 😊
