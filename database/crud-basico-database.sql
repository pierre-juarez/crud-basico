-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-01-2021 a las 01:34:38
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crud-basico`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `ELIMINAR_DOCENTES`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ELIMINAR_DOCENTES` (IN `parametroCODIGO` INT)  DELETE FROM docentes WHERE ID=parametroCODIGO$$

DROP PROCEDURE IF EXISTS `LISTAR_DOCENTES`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `LISTAR_DOCENTES` (IN `parametroCODIGO` INT)  BEGIN 

	IF (parametroCODIGO != 0) THEN
		SELECT * FROM docentes WHERE ID=parametroCODIGO;
	ELSE
		SELECT * FROM DOCENTES;
	END IF;


END$$

DROP PROCEDURE IF EXISTS `MANTENIMIENTO_DOCENTES`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MANTENIMIENTO_DOCENTES` (IN `parametroCODIGO` INT, IN `parametroPROFESOR` VARCHAR(200), IN `parametroASIGNATURA` VARCHAR(100), IN `parametroTUTORIA` VARCHAR(100), IN `parametroINSTRUCCION` VARCHAR(100))  BEGIN 

		IF (parametroCODIGO != 0) THEN		
			UPDATE DOCENTES SET PROFESOR=parametroPROFESOR, TUTORIA=parametroTUTORIA, ASIGNATURA=parametroASIGNATURA,
			INSTRUCCION = parametroINSTRUCCION where ID=parametroCODIGO;			
		ELSE
			INSERT INTO docentes (ID, PROFESOR, ASIGNATURA, TUTORIA, INSTRUCCION) VALUES (null, parametroPROFESOR, parametroASIGNATURA, 			parametroTUTORIA, parametroINSTRUCCION );
		END IF;


END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docentes`
--

DROP TABLE IF EXISTS `docentes`;
CREATE TABLE `docentes` (
  `ID` int(11) NOT NULL,
  `PROFESOR` varchar(200) NOT NULL,
  `ASIGNATURA` varchar(100) NOT NULL,
  `TUTORIA` varchar(100) NOT NULL,
  `INSTRUCCION` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `docentes`
--
ALTER TABLE `docentes`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `docentes`
--
ALTER TABLE `docentes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
